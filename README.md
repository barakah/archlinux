### Advanced Arch Linux Installation Guide
**Goals**:
* Secured and Full system encrypted.
* Modern file system for scaling and roll back to a previous state.

**Objectives:**
* Boot environment with UEFI mode.
* Define 2 disks or more to be encrypted.
* Create btrfs file system on stacked block containers.
* Create file system layouts for snapshots and roll back.
* Install essential packages and management tools.
* Specify system configuration.
