# Verify UEFI boot mode
efibootmgr

# Check the internet connectivity
ip link
ping archlinux.org

# Update the system clock
timedatectl set-ntp true

# Recognize the disks (SSD/HDD) to be encrypted and used for btrfs system
lsblk

# On First Disk (/dev/sda): Create boot ESP partition (512MiB) (example: /dev/sda1) and give it label boot
parted /dev/sda mktable gpt
parted /dev/sda mkpart primary fat32 1 512M
parted /dev/sda set 1 esp on
mkfs.fat -F32 -n boot /dev/sda1

# On First Disk (/dev/sda): Create second partition of remaining disk space (example: /dev/sda2)
parted /dev/sda mkpart primary ext4 512M 100%

# Encrypted system using a detached LUKS header

#Create detached header file
dd if=/dev/zero of=header.img bs=16M count=1
chmod 600 header.img

# On First Disk Encrypt Second partition (/dev/sda2)
cryptsetup luksFormat /dev/sda2 --offset 32768 --header header.img

# On Second Disk Encrypt whole disk
cryptsetup luksFormat /dev/sdb --offset 32768 --header header.img

# Open containers: 
cryptsetup open --header header.img /dev/sda2 btrfs_1
cryptsetup open --header header.img /dev/sdb btrfs_2

# Create btrfs filesystem pool devices (/dev/mapper/btrfs_1 and /dev/mapper/btrfs_2) and give it label btrfs_pool
mkfs.btrfs -L btrfs_pool /dev/mapper/btrfs_1 /dev/mapper/btrfs_2

# Create and mount BTRFS subvolumes
mount -L btrfs_pool /mnt

btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@snapshots
umount /mnt

mount -L btrfs_pool -o subvol=@ /mnt
mkdir -p /mnt/{boot,home,.snapshots}
mount -L btrfs_pool -o subvol=@home /mnt/home                   
mount -L btrfs_pool -o subvol=@snapshots /mnt/.snapshots
mount -L boot /mnt/boot

# Update mirror servers for pacman to speed up downloading packages
pacman -Sy
pacman -S reflector --noconfirm
reflector --verbose -p https --sort rate --save /etc/pacman.d/mirrorlist

# Install essential packages and firmware for common hardware
pacstrap /mnt base linux linux-firmware btrfs-progs vim zsh grml-zsh-config dhcpcd snapper

# move LUKS detached header file "header.img" to boot directory
mv header.img /mnt/boot
genfstab -L /mnt >> /mnt/etc/fstab

# Configure the system
arch-chroot /mnt /bin/zsh
chsh -s /bin/zsh
ln -sf /usr/share/zoneinfo/Asia/Kuwait /etc/localtime
hwclock --systohc

# Uncomment en_US.UTF-8 UTF-8 and other needed locales in /etc/locale.gen, and generate them with:

sed -i  's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
locale-gen

# Create the locale.conf(5) file, and set the LANG variable accordingly:

echo LANG=en_US.UTF-8 > /etc/locale.conf

# Network configuration

echo vm > /etc/hostname

# Add matching entries to hosts(5):

cat >> /etc/hosts << EOL

127.0.0.1	localhost
::1		localhost
127.0.1.1	vm.localdomain	vm

EOL

# Unlock disks (/dev/sda2 and /dev/sdb) from initramfs before system initializing. 
cat >> /etc/crypttab.initramfs << EOL
# <name>	<device>	<password>	<options>

btrfs_1	/dev/sda2	none	header=/boot/header.img
btrfs_2	/dev/sdb	none	header=/boot/header.img
EOL

# Modify /etc/mkinitcpio.conf to use systemd hook and add the header to FILES.
sed -i  's+FILES=()+FILES=(/boot/header.img)+' /etc/mkinitcpio.conf

sed -i  's+HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)+HOOKS=(base systemd autodetect keyboard modconf block sd-encrypt btrfs filesystems fsck)+' /etc/mkinitcpio.conf

mkinitcpio -p linux

# Install and configure Boot loader.
bootctl install
echo "default arch" > /boot/loader/loader.conf
cat >> /boot/loader/entries/arch.conf << EOL
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=/dev/mapper/btrfs_1 rootflags=subvol=@ rw
EOL

# Set the root password:
passwd

# Enable dhcpcd client.
systemctl enable dhcpcd

# Reboot
exit
umount -R /mnt
reboot
